import { ChangeDetectionStrategy, Component } from '@angular/core';
import { CreateScrewModalComponent } from 'src/app/modules/screws/components/create-screw-modal/create-screw-modal.component';
import { ModalService } from 'src/app/shared/components/modal/modal.service';
import { ModalSize } from 'src/app/shared/components/modal/types/modal.types';

@Component({
  selector: 'app-footer-elements',
  templateUrl: './footer-elements.component.html',
  styleUrls: ['./footer-elements.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FooterElementsComponent {
  constructor(private modalService: ModalService) {}

  public openModal(): void {
    const modalConfig = {
      size: ModalSize.L,
      clickOutside: false,
      data: {},
    };
    this.modalService.open(CreateScrewModalComponent, modalConfig);
  }
}
