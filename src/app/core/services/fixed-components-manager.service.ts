import { Injectable } from '@angular/core';

/**
 * Service to centralize all fixed components and be able to manage correctly classes of body and
 * implement better their styles, like: body { overflow: hidden }
 */
@Injectable({ providedIn: 'root' })
export class FixedComponentsManagerService {
  private openedComponents: { [className: string]: number };

  constructor() {
    this.openedComponents = {};
  }

  public created(className: string): void {
    this.openedComponents[className] = this.openedComponents[className]
      ? this.openedComponents[className] + 1
      : 1;
    if (this.openedComponents[className] === 1) {
      this.setBodyClass(className);
    }
  }

  public destroyed(className: string): void {
    this.openedComponents[className] = this.openedComponents[className]
      ? this.openedComponents[className] - 1
      : 0;
    if (this.openedComponents[className] === 0) {
      this.setBodyClass(className, true);
    }
  }

  private setBodyClass(className: string, remove: boolean = false): void {
    document.body.classList[remove ? 'remove' : 'add'](className);
  }
}
