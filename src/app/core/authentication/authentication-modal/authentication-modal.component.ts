import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { catchError, Subject, switchMap, takeUntil } from 'rxjs';
import { showError, ShowErrorFn } from 'src/app/helpers/utils';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { ModalRef } from 'src/app/shared/components/modal/modal-ref';
import { AuthenticationFormName } from '../types/authentication.types';
import { ToastrService } from 'ngx-toastr';
import { ToastStatus } from 'src/app/modules/screws/components/screws-table/types/screws-table.types';

@Component({
  selector: 'app-authentication-modal',
  templateUrl: './authentication-modal.component.html',
  styleUrls: ['./authentication-modal.component.scss'],
})
export class AuthenticationModalComponent implements OnInit, OnDestroy {
  public authenticationFormName: typeof AuthenticationFormName;
  public form: FormGroup;
  public showError: ShowErrorFn;

  private componentDestroyed$: Subject<void>;

  constructor(
    private modalRef: ModalRef,
    private formBuilder: FormBuilder,
    private authService: AuthenticationService,
    private toastr: ToastrService,
    private translateService: TranslateService,
  ) {
    this.authenticationFormName = AuthenticationFormName;
    this.componentDestroyed$ = new Subject<void>();
    this.showError = showError;
    this.form = this.buildForm();
  }

  public ngOnInit(): void {}

  public ngOnDestroy(): void {
    this.componentDestroyed$.next();
    this.componentDestroyed$.complete();
  }

  public onAccept(): void {
    if (!this.form.valid) {
      this.form.markAllAsTouched();
      return;
    }

    const username = this.form.get(this.authenticationFormName.USER_NAME).value;
    const password = this.form.get(this.authenticationFormName.PASSWORD).value;

    this.authService
      .login(username, password)
      .pipe(
        catchError((err) => {
          this.toastr.error(
            this.translateService.instant('authentication-modal.login-error'),
            ToastStatus.ERROR,
          );
          return err;
        }),
        switchMap(() => this.authService.getUser()),
        takeUntil(this.componentDestroyed$),
      )
      .subscribe((user: string) => {
        this.modalRef.close(user);
      });
  }

  public onClose(): void {
    this.modalRef.close();
  }

  private buildForm(): FormGroup {
    return this.formBuilder.group({
      [AuthenticationFormName.USER_NAME]: [null, Validators.required],
      [AuthenticationFormName.PASSWORD]: [null, Validators.required],
    });
  }
}
