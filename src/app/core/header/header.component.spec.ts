import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { TranslateModule, TranslateService } from '@ngx-translate/core';

import { HeaderComponent } from './header.component';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [HeaderComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have the "header" class', () => {
    const header = fixture.debugElement.query(By.css('header'));
    expect(header.nativeElement.classList.contains('header')).toBeTruthy();
  });

  it('should have a link with the "logo" class and the "href" property corresponding to "homeUrl"', () => {
    const logoLink = fixture.debugElement.query(By.css('.logo'));
    expect(logoLink.nativeElement.classList.contains('logo')).toBeTruthy();
    expect(logoLink.nativeElement.href).toEqual(component.homeUrl);
  });

  it('should have an authentication button with the "header-button" class and text corresponding to "mercadona.header.option.authentication"', () => {
    const authButton = fixture.debugElement.query(By.css('#mercadona_header_authentication'));
    expect(authButton.nativeElement.classList.contains('header-button')).toBeTruthy();
    expect(authButton.nativeElement.innerText.trim().split('\n')[0]).toEqual(
      'mercadona.header.option.authentication',
    );
  });
});
