import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { catchError, filter, Subject, takeUntil } from 'rxjs';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { ModalService } from 'src/app/shared/components/modal/modal.service';
import { ModalSize } from 'src/app/shared/components/modal/types/modal.types';
import { config } from '../../app.config';
import { AuthenticationModalComponent } from '../authentication/authentication-modal/authentication-modal.component';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { ToastStatus } from 'src/app/modules/screws/components/screws-table/types/screws-table.types';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent implements OnInit, OnDestroy {
  public homeUrl: string;
  public logoUrl: string;
  public userName: string;

  private componentDestroyed$: Subject<void>;

  constructor(
    private router: Router,
    private modalService: ModalService,
    private cd: ChangeDetectorRef,
    private authService: AuthenticationService,
    private toastr: ToastrService,
    private translateService: TranslateService,
  ) {
    this.componentDestroyed$ = new Subject<void>();
    this.homeUrl = config.routes.home;
    this.logoUrl = `url('../../../assets/img/logo_mercadona.png')`;
  }

  ngOnInit(): void {
    this.getUser();
  }

  public ngOnDestroy(): void {
    this.componentDestroyed$.next();
    this.componentDestroyed$.complete();
  }

  public onOpenModal(): void {
    const modalConfig = {
      size: ModalSize.M,
      clickOutside: false,
      data: {},
    };
    this.modalService
      .open(AuthenticationModalComponent, modalConfig)
      .afterClosed.pipe(
        filter((val) => !!val),
        takeUntil(this.componentDestroyed$),
      )
      .subscribe((value: string) => {
        this.userName = value;
        this.cd.detectChanges();
      });
  }

  public getUser(): void {
    this.authService
      .getUser()
      .pipe(
        catchError((err) => {
          this.toastr.error(
            this.translateService.instant('header.get-user-error'),
            ToastStatus.ERROR,
          );
          return err;
        }),
        takeUntil(this.componentDestroyed$),
      )
      .subscribe((value: string) => {
        this.userName = value;
        this.cd.detectChanges();
      });
  }

  public onLogout(): void {
    this.authService
      .logout()
      .pipe(
        catchError((err) => {
          this.toastr.error(
            this.translateService.instant('header.logout-error'),
            ToastStatus.ERROR,
          );
          return err;
        }),
        takeUntil(this.componentDestroyed$),
      )
      .subscribe((value: string) => {
        this.userName = value;
        this.cd.detectChanges();
        this.navegateToHome();
      });
  }

  public navegateToHome(): void {
    this.router.navigate([this.homeUrl]);
  }
}
