import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { TranslateModule } from '@ngx-translate/core';
import { ModalModule } from '../shared/components/modal/modal.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthenticationModalComponent } from './authentication/authentication-modal/authentication-modal.component';
import { FieldErrorModule } from '../shared/components/field-error/field-error.module';
import { FooterComponent } from './footer/footer.component';
import { FooterElementsComponent } from './footer/components/footer-elements/footer-elements.component';
import { ToastrModule } from 'ngx-toastr';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  declarations: [
    HeaderComponent,
    AuthenticationModalComponent,
    FooterComponent,
    FooterElementsComponent,
  ],
  imports: [
    CommonModule,
    TranslateModule,
    ModalModule,
    ReactiveFormsModule,
    FieldErrorModule,
    FormsModule,
    ToastrModule.forRoot(),
    NgxSpinnerModule,
  ],
  exports: [
    HeaderComponent,
    AuthenticationModalComponent,
    FooterComponent,
    FooterElementsComponent,
    FooterComponent,
  ],
})
export class CoreModule {}
