import { environment } from 'src/environments/environment';
import { IAppConfig } from './types/app-config.types';

export const config: IAppConfig = {
  appId: 'technical-test-mercadona',
  routes: {
    home: 'home',
    screws: 'screws',
  },
  endpoints: {
    home: environment.baseApiUrl + '/',
    screws: environment.baseApiUrl + '/',
  },
};
