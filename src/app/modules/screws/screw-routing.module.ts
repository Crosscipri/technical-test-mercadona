import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ScrewsComponent } from './components/screws/screws.component';

const routes: Routes = [
  {
    path: '',
    component: ScrewsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ScrewRoutingModule {}
