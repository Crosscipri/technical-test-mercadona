import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ScrewsComponent } from './components/screws/screws.component';
import { ScrewRoutingModule } from './screw-routing.module';
import { TranslateModule } from '@ngx-translate/core';
import { ScrewsTableComponent } from './components/screws-table/screws-table.component';
import { TableConfigurationModalComponent } from './components/screws-table/components/table-configuration-modal/table-configuration-modal.component';
import { ModalModule } from 'src/app/shared/components/modal/modal.module';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { CreateScrewModalComponent } from './components/create-screw-modal/create-screw-modal.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DeleteScrewModalComponent } from './components/screws-table/components/delete-screw-modal/delete-screw-modal.component';
import { ToastrModule } from 'ngx-toastr';
import { NgxPaginationModule } from 'ngx-pagination';
import { FieldErrorModule } from 'src/app/shared/components/field-error/field-error.module';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  declarations: [
    ScrewsComponent,
    ScrewsTableComponent,
    TableConfigurationModalComponent,
    CreateScrewModalComponent,
    DeleteScrewModalComponent,
  ],
  imports: [
    CommonModule,
    ScrewRoutingModule,
    TranslateModule,
    ModalModule,
    ReactiveFormsModule,
    DragDropModule,
    NgxPaginationModule,
    FormsModule,
    FieldErrorModule,
    ToastrModule.forRoot(),
    NgxSpinnerModule,
  ],
})
export class ScrewsModule {}
