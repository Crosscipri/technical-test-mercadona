import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject, takeUntil } from 'rxjs';
import { showError, ShowErrorFn } from 'src/app/helpers/utils';
import { ScrewService } from 'src/app/services/screws.service';
import { ModalRef } from 'src/app/shared/components/modal/modal-ref';
import { CreateScrewFormName, ISelect } from 'src/app/types/screws.types';

@Component({
  selector: 'app-create-screw-modal',
  templateUrl: './create-screw-modal.component.html',
  styleUrls: ['./create-screw-modal.component.scss'],
})
export class CreateScrewModalComponent implements OnInit, OnDestroy {
  public createScrewFormName: typeof CreateScrewFormName;
  public form: FormGroup;
  public showError: ShowErrorFn;
  public formatTypes: ISelect[];

  private componentDestroyed$: Subject<void>;

  constructor(
    private modalRef: ModalRef,
    private formBuilder: FormBuilder,
    private screwService: ScrewService,
  ) {
    this.createScrewFormName = CreateScrewFormName;
    this.componentDestroyed$ = new Subject<void>();
    this.showError = showError;
    this.form = this.buildForm();
    this.formatTypes = [];
  }

  public ngOnInit(): void {
    this.getScrewsFormatTypes();
  }

  public ngOnDestroy(): void {
    this.componentDestroyed$.next();
    this.componentDestroyed$.complete();
  }

  public onAccept(): void {
    if (!this.form.valid) {
      this.form.markAllAsTouched();
      return;
    }

    this.screwService
      .createScrew(this.form.getRawValue())
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(() => {
        this.onClose();
      });
  }

  public onClose(): void {
    this.modalRef.close();
  }

  private buildForm(): FormGroup {
    return this.formBuilder.group({
      [CreateScrewFormName.NAME]: [null, Validators.required],
      [CreateScrewFormName.PRICE]: [null, Validators.required],
      [CreateScrewFormName.FORMAT]: [null, Validators.required],
      [CreateScrewFormName.BRAND]: [null, Validators.required],
    });
  }

  private getScrewsFormatTypes(): void {
    this.screwService
      .getScrewsFormatTypes()
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe((value) => {
        this.formatTypes = value;
      });
  }
}
