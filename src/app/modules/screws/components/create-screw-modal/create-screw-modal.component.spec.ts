import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateScrewModalComponent } from './create-screw-modal.component';

describe('CreateScrewModalComponent', () => {
  let component: CreateScrewModalComponent;
  let fixture: ComponentFixture<CreateScrewModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateScrewModalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CreateScrewModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
