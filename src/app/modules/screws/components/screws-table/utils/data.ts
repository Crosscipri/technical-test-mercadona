import { ColumnElements, IColumns } from '../types/screws-table.types';
import { marker as _ } from '@biesbjerg/ngx-translate-extract-marker';

export function getColumnsInfo(): IColumns[] {
  return [
    {
      label: _('screws-table.header.name'),
      key: ColumnElements.NAME,
      visible: true,
      fixed: false,
    },
    {
      label: _('screws-table.header.price'),
      key: ColumnElements.PRICE,
      visible: true,
      fixed: false,
    },
    {
      label: _('screws-table.header.format'),
      key: ColumnElements.FORMAT,
      visible: true,
      fixed: false,
    },
    {
      label: _('screws-table.header.brand'),
      key: ColumnElements.BRAND,
      visible: true,
      fixed: false,
    },
    {
      label: _('screws-table.header.actions'),
      key: ColumnElements.ACTIONS,
      visible: true,
      fixed: true,
    },
  ];
}
