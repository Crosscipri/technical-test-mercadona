import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ColumnElements, IColumns, ToastStatus } from './types/screws-table.types';
import { getColumnsInfo } from './utils/data';
import { ModalSize } from 'src/app/shared/components/modal/types/modal.types';
import { ModalService } from 'src/app/shared/components/modal/modal.service';
import { TableConfigurationModalComponent } from './components/table-configuration-modal/table-configuration-modal.component';
import { IScrew } from 'src/app/types/screws.types';
import { ScrewService } from 'src/app/services/screws.service';
import { catchError, finalize, Subject, takeUntil } from 'rxjs';
import { DeleteScrewModalComponent } from './components/delete-screw-modal/delete-screw-modal.component';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-screws-table',
  templateUrl: './screws-table.component.html',
  styleUrls: ['./screws-table.component.scss'],
})
export class ScrewsTableComponent implements OnInit, OnDestroy {
  public columnElements: typeof ColumnElements;
  public columns: IColumns[];
  public columnsData: IScrew[];
  public itemsPerPage: number;
  public currentPage: number;
  public totalItems: number;

  private componentDestroyed$: Subject<void>;

  constructor(
    private modalService: ModalService,
    private screwService: ScrewService,
    private toastr: ToastrService,
    private translateService: TranslateService,
    private spinnerService: NgxSpinnerService,
  ) {
    this.columnElements = ColumnElements;
    this.columns = getColumnsInfo();
    this.componentDestroyed$ = new Subject<void>();
    this.columnsData = [];
    this.itemsPerPage = 20;
    this.currentPage = 1;
  }

  public ngOnInit(): void {
    this.getScrews();
    this.updateTableData();
  }

  public ngOnDestroy(): void {
    this.componentDestroyed$.next();
    this.componentDestroyed$.complete();
  }

  public openModal(): void {
    const modalConfig = {
      size: ModalSize.M,
      clickOutside: false,
      data: this.columns,
    };
    this.modalService
      .open(TableConfigurationModalComponent, modalConfig)
      .afterClosed.pipe(takeUntil(this.componentDestroyed$))
      .subscribe((value) => {
        this.columns = value;
      });
  }

  public deleteScrew(item: IScrew): void {
    const modalConfig = {
      size: ModalSize.M,
      clickOutside: false,
      data: this.columns,
    };
    this.modalService
      .open(DeleteScrewModalComponent, modalConfig)
      .afterClosed.subscribe((value) => {
        if (value)
          this.screwService
            .deleteScrews(item.id)
            .pipe(
              catchError((err) => {
                this.toastr.error(
                  this.translateService.instant('screw-table.delete-error'),
                  ToastStatus.ERROR,
                );
                return err;
              }),
              takeUntil(this.componentDestroyed$),
            )
            .subscribe(() => {
              this.toastr.success(
                this.translateService.instant('screw-table.delete-success'),
                ToastStatus.SUCCESS,
              );
              this.getScrews();
            });
      });
  }

  public onTableDataChange(event: any) {
    this.currentPage = event;
    this.getScrews();
  }

  public onTableSizeChange(event: any): void {
    this.itemsPerPage = event.target.value;
    this.currentPage = 1;
    this.getScrews();
  }

  public onItemsPerPageChange() {}

  private getScrews(): void {
    this.spinnerService.show();
    this.screwService
      .getScrews()
      .pipe(
        catchError((err) => {
          this.toastr.error(
            this.translateService.instant('screw-table.get-screws-error'),
            ToastStatus.ERROR,
          );
          return err;
        }),
        finalize(() => {
          this.spinnerService.hide();
        }),
        takeUntil(this.componentDestroyed$),
      )
      .subscribe((value: IScrew[]) => {
        this.totalItems = value.length;
        this.columnsData = value;
      });
  }

  private updateTableData(): void {
    this.screwService
      .getScrewsSubject()
      .pipe(
        catchError((err) => {
          this.toastr.error(
            this.translateService.instant('screw-table.update-data-error'),
            ToastStatus.ERROR,
          );
          return err;
        }),
        takeUntil(this.componentDestroyed$),
      )
      .subscribe((value: IScrew[]) => (this.columnsData = value));
  }
}
