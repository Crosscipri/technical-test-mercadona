import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteScrewModalComponent } from './delete-screw-modal.component';

describe('DeleteScrewModalComponent', () => {
  let component: DeleteScrewModalComponent;
  let fixture: ComponentFixture<DeleteScrewModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeleteScrewModalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DeleteScrewModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
