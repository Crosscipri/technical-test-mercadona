import { Component } from '@angular/core';
import { ModalRef } from 'src/app/shared/components/modal/modal-ref';

@Component({
  selector: 'app-delete-screw-modal',
  templateUrl: './delete-screw-modal.component.html',
  styleUrls: ['./delete-screw-modal.component.scss'],
})
export class DeleteScrewModalComponent {
  constructor(private modalRef: ModalRef) {}

  public onClose(): void {
    this.modalRef.close();
  }

  public onAccept(): void {
    this.modalRef.close(true);
  }
}
