import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { IColumns } from '../../types/screws-table.types';
import { ModalConfig } from 'src/app/shared/components/modal/modal.config';
import { ModalRef } from 'src/app/shared/components/modal/modal-ref';

@Component({
  selector: 'app-table-configuration-modal',
  templateUrl: './table-configuration-modal.component.html',
  styleUrls: ['./table-configuration-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TableConfigurationModalComponent implements OnInit {
  public columns: IColumns[];
  public modifiedColumns: IColumns[];
  constructor(private modalConfig: ModalConfig, private modalRef: ModalRef) {
    this.columns = this.modalConfig.data;
    this.modifiedColumns = JSON.parse(JSON.stringify(this.columns));
  }

  public ngOnInit(): void {}

  public toggleColumnVisibility(column: IColumns, index: number): boolean {
    return (this.modifiedColumns[index].visible = !column.visible);
  }

  public toggleColumnFixedPosition(column: IColumns, index: number): void {
    column.fixed = !column.fixed;
  }

  public canSwapColumns(event: CdkDragDrop<any>) {
    const targetIndex = event.currentIndex;
    const targetColumn = this.columns[targetIndex];
    return !targetColumn.fixed;
  }

  public onColumnDrop(event: CdkDragDrop<IColumns[]>): void {
    const previousIndex = event.previousIndex;
    const currentIndex = event.currentIndex;

    const targetColumn = this.columns[currentIndex];
    if (targetColumn.fixed) {
      // Move the dragged column back to its original position
      moveItemInArray(this.modifiedColumns, currentIndex, previousIndex);
      return;
    }

    moveItemInArray(this.modifiedColumns, previousIndex, currentIndex);
    this.columns = this.modifiedColumns;
  }

  public onAccept(): void {
    this.modalRef.close(this.modifiedColumns);
  }

  public onClose(): void {
    this.modalRef.close(this.columns);
  }
}
