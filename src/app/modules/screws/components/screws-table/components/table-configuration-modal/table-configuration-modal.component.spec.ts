import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableConfigurationModalComponent } from './table-configuration-modal.component';

describe('TableConfigurationModalComponent', () => {
  let component: TableConfigurationModalComponent;
  let fixture: ComponentFixture<TableConfigurationModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TableConfigurationModalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TableConfigurationModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
