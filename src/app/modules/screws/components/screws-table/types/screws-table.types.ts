export interface IColumns {
  label: string;
  key: string;
  visible: boolean;
  fixed: boolean;
}

export enum ColumnElements {
  NAME = 'name',
  PRICE = 'price',
  FORMAT = 'format',
  BRAND = 'brand',
  ACTIONS = 'actions',
}

export enum ToastStatus {
  SUCCESS = 'Success',
  ERROR = 'Error',
}
