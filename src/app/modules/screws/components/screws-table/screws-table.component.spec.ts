import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScrewsTableComponent } from './screws-table.component';

describe('ScrewsTableComponent', () => {
  let component: ScrewsTableComponent;
  let fixture: ComponentFixture<ScrewsTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScrewsTableComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ScrewsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
