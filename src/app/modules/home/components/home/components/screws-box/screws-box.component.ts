import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { catchError, Subject, takeUntil } from 'rxjs';
import { config } from 'src/app/app.config';
import { ToastStatus } from 'src/app/modules/screws/components/screws-table/types/screws-table.types';
import { ScrewService } from 'src/app/services/screws.service';
import { IScrew } from 'src/app/types/screws.types';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-screws-box',
  templateUrl: './screws-box.component.html',
  styleUrls: ['./screws-box.component.scss'],
})
export class ScrewsBoxComponent implements OnInit, OnDestroy {
  public screwsUrl: string;
  public totalItems: number;

  private componentDestroyed$: Subject<void>;
  constructor(
    private router: Router,
    private screwService: ScrewService,
    private toastr: ToastrService,
    private translateService: TranslateService,
  ) {
    this.screwsUrl = config.routes.screws;
    this.componentDestroyed$ = new Subject<void>();
  }

  public ngOnInit(): void {
    this.getScrews();
  }

  public ngOnDestroy(): void {
    this.componentDestroyed$.next();
    this.componentDestroyed$.complete();
  }

  public navegateToScrews(): void {
    this.router.navigate([this.screwsUrl]);
  }

  private getScrews(): void {
    this.screwService
      .getScrews()
      .pipe(
        catchError((err) => {
          this.toastr.error(
            this.translateService.instant('screws-box.get-screws-error'),
            ToastStatus.ERROR,
          );
          return err;
        }),
        takeUntil(this.componentDestroyed$),
      )
      .subscribe((value: IScrew[]) => {
        this.totalItems = value.length;
      });
  }
}
