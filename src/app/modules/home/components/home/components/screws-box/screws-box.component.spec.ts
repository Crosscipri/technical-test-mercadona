import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScrewsBoxComponent } from './screws-box.component';

describe('ScrewsBoxComponent', () => {
  let component: ScrewsBoxComponent;
  let fixture: ComponentFixture<ScrewsBoxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScrewsBoxComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ScrewsBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
