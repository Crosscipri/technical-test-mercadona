import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './components/home/home.component';
import { TranslateModule } from '@ngx-translate/core';
import { HomeRoutingModule } from './home-routing.module';
import { ScrewsBoxComponent } from './components/home/components/screws-box/screws-box.component';
import { ToastrModule } from 'ngx-toastr';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  declarations: [HomeComponent, ScrewsBoxComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    TranslateModule,
    ToastrModule.forRoot(),
    NgxSpinnerModule,
  ],
  exports: [HomeComponent],
})
export class HomeModule {}
