import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { config } from './app.config';
import { AuthGuard } from './helpers/guards/auth.guard';
1;
const routes: Routes = [
  {
    path: config.routes.home,
    loadChildren: () => import('./modules/home/home.module').then((m) => m.HomeModule),
  },
  {
    path: config.routes.screws,
    loadChildren: () => import('./modules/screws/screws.module').then((m) => m.ScrewsModule),
    canActivate: [AuthGuard],
  },
  {
    path: '**',
    redirectTo: `/${config.routes.home}`,
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
