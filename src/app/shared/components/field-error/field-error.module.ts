import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { FieldErrorComponent } from './field-error.component';

@NgModule({
  exports: [FieldErrorComponent],
  declarations: [FieldErrorComponent],
  imports: [CommonModule, ReactiveFormsModule],
})
export class FieldErrorModule {}
