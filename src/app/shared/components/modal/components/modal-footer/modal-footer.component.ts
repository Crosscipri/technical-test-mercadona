import { Component, ChangeDetectionStrategy, EventEmitter, Output, Input } from '@angular/core';

@Component({
  selector: 'merca-modal-footer',
  templateUrl: './modal-footer.component.html',
  styleUrls: ['./modal-footer.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ModalFooterComponent {
  @Input()
  public primaryText?: string;
  @Input()
  public primaryDisabled?: boolean;
  @Input()
  public primaryType: 'button' | 'submit';
  @Input()
  public secondaryText?: string;
  @Input()
  public secondaryDisabled?: boolean;
  @Input()
  public secondaryType: 'button' | 'submit';

  @Output()
  public primaryClick: EventEmitter<void> = new EventEmitter();
  @Output()
  public secondaryClick: EventEmitter<void> = new EventEmitter();

  constructor() {
    this.primaryType = 'button';
    this.secondaryType = 'button';
  }
}
