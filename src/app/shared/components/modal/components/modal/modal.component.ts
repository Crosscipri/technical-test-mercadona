import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnDestroy,
  AfterViewInit,
  ComponentRef,
  ViewChild,
  Type,
  ComponentFactoryResolver,
  ChangeDetectorRef,
  OnInit,
} from '@angular/core';
import { ModalRef } from '../../modal-ref';
import { InsertionDirective } from '../../modal.directive';
import { ModalSize } from '../../types/modal.types';

@Component({
  selector: 'merca-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ModalComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input()
  public overlay?: boolean;
  @Input()
  public overlayOpaque?: boolean;
  @Input()
  public modalSize?: ModalSize;
  @Input()
  public modalPadding?: boolean;
  @Input()
  public clickOutside?: boolean;
  @Input()
  public closeWithEsc?: boolean;
  @Input()
  public modalResult: any;
  @Input()
  public data: any;

  @ViewChild(InsertionDirective)
  public insertionPoint?: InsertionDirective;

  public childModalType: any;
  public modalComponentRef?: ComponentRef<any>;
  public modalClasses?: Array<string>;
  public insideModal?: boolean;

  constructor(
    private modalFactoryResolver: ComponentFactoryResolver,
    private cd: ChangeDetectorRef,
    public modalRef: ModalRef,
  ) {}

  public ngOnInit(): void {
    this.setClasses();
  }

  public ngAfterViewInit(): void {
    this.loadChildComponent(this.childModalType);
    this.cd.detectChanges();
  }

  public ngOnDestroy(): void {
    if (this.modalComponentRef) {
      this.modalComponentRef.destroy();
    }
  }

  public onOverlayClicked(): void {
    if (this.clickOutside && !this.insideModal) {
      this.close();
    }
    this.insideModal = false;
  }

  public close(): void {
    this.modalRef.close(this.modalResult);
  }

  public onPressEsc(): void {
    if (this.closeWithEsc) {
      this.close();
    }
  }

  public onDialogClicked(evt: MouseEvent): void {
    this.insideModal = false;
    evt.stopPropagation();
  }

  private loadChildComponent(componentType: Type<any>): void {
    const modalFactory = this.modalFactoryResolver.resolveComponentFactory(componentType);

    if (this.insertionPoint) {
      const viewContainerRef = this.insertionPoint.viewContainerRef;
      viewContainerRef.clear();

      this.modalComponentRef = viewContainerRef.createComponent(modalFactory);
      this.modalComponentRef.instance.data = this.data;
    }
  }

  private setClasses(): void {
    this.modalClasses = [this.modalSize ?? ModalSize.M];
    if (this.modalPadding) {
      this.modalClasses.push('modal-padding');
    }
  }
}
