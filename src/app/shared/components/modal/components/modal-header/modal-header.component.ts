import { Component, ChangeDetectionStrategy, Input } from '@angular/core';

@Component({
  selector: 'merca-modal-header',
  templateUrl: './modal-header.component.html',
  styleUrls: ['./modal-header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ModalHeaderComponent {
  @Input()
  public title?: string;
  @Input()
  public modalResult: any;
  @Input()
  public hideCloseButton?: boolean;

  constructor() {}

  public clickClose(event: MouseEvent): void {
    (event as MouseEvent).stopImmediatePropagation();
    (event as MouseEvent).preventDefault();
  }
}
