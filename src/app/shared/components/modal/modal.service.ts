import {
  Injectable,
  Type,
  ApplicationRef,
  ComponentFactoryResolver,
  Injector,
  Inject,
  ComponentRef,
} from '@angular/core';
import { ModalComponent } from './components/modal/modal.component';
import { DOCUMENT } from '@angular/common';
import { ModalRef } from './modal-ref';
import { FixedComponentsManagerService } from 'src/app/core/services/fixed-components-manager.service';
import { ModalConfig } from './modal.config';
import { ModalInjector } from './modal.injector';
import { ModalSize } from './types/modal.types';

@Injectable()
export class ModalService {
  private modals: Map<string, ComponentRef<ModalComponent>>;
  private bodyClass: string;

  constructor(
    private fixedComponentsManagerService: FixedComponentsManagerService,
    private appRef: ApplicationRef,
    private componentFactoryResolver: ComponentFactoryResolver,
    private injector: Injector,
    @Inject(DOCUMENT) private document: any,
  ) {
    this.modals = new Map();
    this.bodyClass = 'has-modal';
  }

  public open<DATA = any, RESULT = any>(
    content: Type<any>,
    modalConfig: ModalConfig<DATA>,
  ): ModalRef<RESULT> {
    const modalId = `modal-${Date.now()}#${this.modals.size + 1}`;
    let containerModalRef: ComponentRef<ModalComponent>;
    const wMap = new WeakMap();
    wMap.set(ModalConfig, modalConfig);

    const modalRef = new ModalRef(modalId);
    wMap.set(ModalRef, modalRef);

    const subAfterClosed = modalRef.afterClosed.subscribe(() => {
      this.close(modalId);
      subAfterClosed.unsubscribe();
    });

    const modalFactory = this.componentFactoryResolver.resolveComponentFactory(ModalComponent);
    containerModalRef = modalFactory.create(new ModalInjector(this.injector, wMap));

    containerModalRef.instance.childModalType = content;

    containerModalRef.instance.overlay = modalConfig.overlay ?? true;
    containerModalRef.instance.overlayOpaque = modalConfig.overlayOpaque ?? false;
    containerModalRef.instance.modalPadding = modalConfig.padding ?? true;
    containerModalRef.instance.modalSize = modalConfig.size ?? ModalSize.L;
    containerModalRef.instance.clickOutside = modalConfig.clickOutside ?? true;
    containerModalRef.instance.closeWithEsc = modalConfig.closeWithEsc ?? true;
    containerModalRef.instance.data = modalConfig.data;
    containerModalRef.instance.modalResult = modalConfig.modalResult;

    this.fixedComponentsManagerService.created(this.bodyClass);
    this.appRef.attachView(containerModalRef.hostView);
    this.document.body.appendChild(containerModalRef.location.nativeElement);

    this.modals.set(modalId, containerModalRef);

    return modalRef;
  }

  public close(id?: string): void {
    if (id) {
      const modalRef = this.modals.get(id);
      if (modalRef) {
        this.modals.delete(id);
        this.destroy(modalRef);
      }
    } else {
      this.closeAll();
    }
  }

  private closeAll(): void {
    this.modals.forEach((modalRef) => {
      if (modalRef) {
        this.destroy(modalRef);
      }
    });
    this.modals.clear();
  }

  private destroy(modalRef: ComponentRef<ModalComponent>): void {
    this.fixedComponentsManagerService.destroyed(this.bodyClass);
    this.appRef.detachView(modalRef.hostView);
    modalRef.destroy();
  }
}
