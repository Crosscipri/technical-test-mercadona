import { NgModule } from '@angular/core';
import { ModalComponent } from './components/modal/modal.component';
import { CommonModule } from '@angular/common';
import { InsertionDirective, ModalCloseDirective } from './modal.directive';
import { ModalHeaderComponent } from './components/modal-header/modal-header.component';
import { ModalFooterComponent } from './components/modal-footer/modal-footer.component';
import { TranslateModule } from '@ngx-translate/core';
import { ModalService } from './modal.service';

@NgModule({
  declarations: [
    ModalComponent,
    InsertionDirective,
    ModalCloseDirective,
    ModalHeaderComponent,
    ModalFooterComponent,
  ],
  imports: [CommonModule, TranslateModule],
  exports: [ModalCloseDirective, ModalHeaderComponent, ModalFooterComponent, ModalComponent],
  providers: [ModalService],
})
export class ModalModule {}
