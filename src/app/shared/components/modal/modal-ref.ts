import { Observable, Subject } from 'rxjs';

export class ModalRef<RESULT = any> {
  public afterClosed: Observable<RESULT>;
  private readonly afterClosedSubject: Subject<RESULT>;

  public updatedData: Observable<RESULT>;
  private readonly updateDataSubject: Subject<RESULT>;

  constructor(public id: string) {
    this.afterClosedSubject = new Subject();
    this.afterClosed = this.afterClosedSubject.asObservable();

    this.updateDataSubject = new Subject();
    this.updatedData = this.updateDataSubject.asObservable();
  }

  public update(result: RESULT): void {
    this.updateDataSubject.next(result);
  }

  public close(result?: RESULT): void {
    this.afterClosedSubject.next(result as RESULT);
    this.afterClosedSubject.complete();
    this.updateDataSubject.complete();
  }
}
