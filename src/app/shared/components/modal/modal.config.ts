import { ModalSize } from './types/modal.types';

export class ModalConfig<D = any> {
  public size?: ModalSize;
  public padding?: boolean;
  public overlay?: boolean;
  public overlayOpaque?: boolean;
  public clickOutside?: boolean;
  public closeWithEsc?: boolean;
  public data?: D;
  public modalResult?: any;
}
