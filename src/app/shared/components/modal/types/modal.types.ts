export enum ModalSize {
  S = 'small',
  M = 'medium',
  L = 'large',
  XL = 'extra-large',
  FS = 'fullscreen',
}
