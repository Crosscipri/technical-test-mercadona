import { Directive, ViewContainerRef, HostListener, Input, HostBinding } from '@angular/core';
import { ModalRef } from './modal-ref';

@Directive({
  selector: '[mercaInsertion]',
})
export class InsertionDirective {
  constructor(public viewContainerRef: ViewContainerRef) {}
}

@Directive({
  selector: '[ModalClose]',
})
export class ModalCloseDirective {
  constructor(private modalRef: ModalRef) {}

  @Input()
  public modalResult: any;

  @HostListener('click')
  public onClick(): void {
    this.modalRef.close(this.modalResult);
  }
}
