import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of, Subject, throwError } from 'rxjs';
import { catchError, delay, tap } from 'rxjs/operators';
import { IScrew, ISelect } from '../types/screws.types';

@Injectable({
  providedIn: 'root',
})
export class ScrewService {
  private apiUrl = 'assets/data-base/screws.json';
  private apiFormatsUrl = 'assets/data-base/format-types.json';
  private screws: IScrew[];
  private readonly SCREWS_KEY = 'screws';
  private screwsSubject = new Subject<IScrew[]>();

  constructor(private http: HttpClient) {
    this.loadProducts().subscribe();
    this.screws = JSON.parse(localStorage.getItem(this.SCREWS_KEY));
  }

  public getScrewsFormatTypes(): Observable<ISelect[]> {
    return this.http.get<ISelect[]>(this.apiFormatsUrl);
  }

  public getScrews(): Observable<IScrew[]> {
    const savedScrews = localStorage.getItem(this.SCREWS_KEY);
    if (savedScrews) {
      return of(JSON.parse(savedScrews)).pipe(delay(500));
    } else {
      return of([]);
    }
  }

  public createScrew(screw: IScrew): Observable<IScrew> {
    return new Observable<IScrew>((observer) => {
      const id = this.getNewId();
      screw.id = id;
      this.screws.push(screw);
      const json = JSON.stringify(this.screws);
      this.saveScrews(json);
      this.screwsSubject.next(this.screws);
      observer.next(screw);
      observer.complete();
    });
  }

  public deleteScrews(id: number): Observable<void> {
    this.screws = this.screws.filter((item) => item.id !== id);
    const json = JSON.stringify(this.screws);
    this.saveScrews(json);
    return new Observable<void>((observer) => {
      observer.next();
      observer.complete();
    });
  }

  public getScrewsSubject(): Observable<IScrew[]> {
    return this.screwsSubject.asObservable();
  }

  private saveScrews(json: string) {
    localStorage.setItem(this.SCREWS_KEY, json);
  }

  private loadProducts(): Observable<IScrew[]> {
    return this.http.get<IScrew[]>(this.apiUrl).pipe(
      tap((screws) => {
        this.screws = screws;
        localStorage.setItem(this.SCREWS_KEY, JSON.stringify(screws));
      }),
    );
  }

  private getNewId(): number {
    const maxId = this.screws.reduce((max, p) => (p.id > max ? p.id : max), 0);
    return maxId + 1;
  }
}
