import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  private loggedIn: boolean;
  private CURRENT_USER_KEY = 'currentUser';

  public login(username: string, password: string): Observable<boolean> {
    this.loggedIn = true;
    localStorage.setItem(this.CURRENT_USER_KEY, username);
    return of(true);
  }

  public logout(): Observable<string> {
    localStorage.removeItem(this.CURRENT_USER_KEY);
    this.loggedIn = false;
    return of(null);
  }

  public getUser(): Observable<string> {
    const username = localStorage.getItem(this.CURRENT_USER_KEY);
    return of(username);
  }

  public isLoggedIn(): boolean {
    const currentUser = localStorage.getItem(this.CURRENT_USER_KEY);
    return this.loggedIn || (currentUser !== null && currentUser !== undefined);
  }
}
