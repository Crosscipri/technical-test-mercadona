export interface ISelect {
  id: number;
  code: string;
  description: string;
}

export interface IScrew {
  id: number;
  name: string;
  price: number;
  format: string;
  brand: string;
}

export enum CreateScrewFormName {
  NAME = 'name',
  PRICE = 'price',
  FORMAT = 'format',
  BRAND = 'brand',
}
