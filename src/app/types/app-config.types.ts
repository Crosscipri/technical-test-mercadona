export interface IAppConfig {
  appId: string;
  routes: {
    home: string;
    screws: string;
  };
  endpoints: {
    home: string;
    screws: string;
  };
}

type LANG = 'es' | 'pt';

export const DEFAULT_LANG: LANG = 'es';
