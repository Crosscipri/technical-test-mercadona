import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { config } from 'src/app/app.config';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  private homeUrl: string;

  constructor(private authService: AuthenticationService, private router: Router) {
    this.homeUrl = config.routes.home;
  }

  canActivate(): boolean {
    if (this.authService.isLoggedIn()) {
      return true;
    } else {
      this.router.navigate([this.homeUrl]);
      return false;
    }
  }
}
