import { AbstractControl } from '@angular/forms';

export type ShowErrorFn = (control: AbstractControl, errorKey: string) => boolean;

/**
 * Function builder to control when show an control error
 */
export function showError(control: AbstractControl, errorKey: string): boolean {
  return control.invalid && control.touched && control.hasError(errorKey);
}
